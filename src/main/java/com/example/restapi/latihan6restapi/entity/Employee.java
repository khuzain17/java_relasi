package com.example.restapi.latihan6restapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

//step - step:
//1. tentukan nama kolom apa saja yang akan dibuat dalam tabel
//2. membuat package entity
//3. memasukkan entity
//4. tambahkan anotasi @Setter, @Getter, @Entity, @Table
//5. di dalam class, tambahkan masing-masing dari nama kolom yang ingin di generate
//6. atur nullabe, name, dan format

@Setter
@Getter
@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false, length = 35)
    private String name;

    @Column(name = "sex", nullable = false, length = 1)
    private char sex;

    @Column(name = "birthDate", nullable = false, length = 10)
    private LocalDate birthDate;

    @Column(name = "address", nullable = false, length = 45)
    private String address;

    @Column(name = "status", nullable = false, length = 1)
    private int status;
}
